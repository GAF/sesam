// This can't be done with a bash/python/#! script hence the setuid bit will be
// ignored
// gcc -o sesamOpen sesamOpen.c
// chown sesam.sesam sesamOpen
// chmod u=rwxs,g=rx,o=r sesamOpen

#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <stdlib.h>

int main(const char* file_name) {
	FILE* file = NULL;
	int pid = 0;
	
	if((file = fopen("/run/sesam.pid", "r")) == NULL) {
		perror("Can't open PID file");
		return EXIT_FAILURE;
	}
	
	if(fscanf(file, "%d", &pid) != 1) {
		perror("Can't find PID in PID file");
		return EXIT_FAILURE;
	}
	
	if(kill(pid, SIGUSR1) != 0) {
		perror("Can't send signal to PID");
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}