<?php
$db = new SQLite3('db');

$db->exec('PRAGMA foreign_keys = ON');

$db->exec('DROP TABLE IF EXISTS "logs"');
$db->exec('DROP TABLE IF EXISTS "users"');

$db->exec('
CREATE TABLE "users" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    "name" TEXT NOT NULL UNIQUE,
    "user" TEXT NOT NULL,
    "pass" TEXT NOT NULL,
    "enabled" INTEGER NOT NULL DEFAULT 1,
    "manager" INTEGER NOT NULL DEFAULT 0,
    "note" TEXT NOT NULL DEFAULT ""
)');

$db->exec('INSERT INTO users (name, user, pass, manager) VALUES ("DAU", "dau", "'.crypt('dau').'", 0)');
$db->exec('INSERT INTO users (name, user, pass, manager) VALUES ("Administrator", "admin", "'.crypt('admin').'", 1)');


$db->exec('
CREATE TABLE "logs" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    "subject" INTEGER NOT NULL,
    "verb" INTEGER NOT NULL,
    "object" INTEGER,
    "data" TEXT,
    "date" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY(subject) REFERENCES users(id),
    FOREIGN KEY(object) REFERENCES users(id)
)');

$db->exec('INSERT INTO logs (subject, verb, object, data) VALUES (1, 1, NULL, NULL)');
$db->exec('INSERT INTO logs (subject, verb, object, data) VALUES (1, 2, 1, \''.SQLite3::escapeString(serialize(array("msg" => 'admin'))).'\')');

// FK Test
// $db->exec('INSERT INTO logs (subject, verb, object, data) VALUES (1, 1, 42, NULL)'); // ERROR
// $db->exec('INSERT INTO logs (subject, verb, object, data) VALUES (42, 1, 1, NULL)'); // ERROR
// $db->exec('INSERT INTO logs (subject, verb, object, data) VALUES (1, 1, NULL, NULL)'); // OK
