<?php

class QueryBuilder {
	private $db = null;
	private $query = "";
	private $bindings = [];
	
	
	public function __construct($db, $query) {
		$this->db = $db;
		$this->query = $query;
	}
	
	public function addBindingList($placeholder, $joint, $prepend = "", $append = "") {
		if(isset($this->bindings[$placeholder])) {
			/// @todo also check for inclusion
			throw new Exception("Placeholder \"$placeholder\" already exists");
		}
		
		$this->bindings[$placeholder] = array(
			"joint" => $joint,
			"bindings" => array(),
			"prepend" => $prepend,
			"append" => $append
		);
	}
	
	public function addBinding($placeholder, $key, $value, $type, $ignoreNull = false) {
		if($value === null && $ignoreNull) {
			return;
		}
		
		// Save the binding
		$this->bindings[$placeholder]["bindings"][$key] = array(
			"value" => $value,
			"type" => $type
		);
	}
	
	public function build() {
		$query = $this->query;
		
		foreach($this->bindings as $placeholder => $bindings) {
			if(count($bindings["bindings"]) === 0) {
				$query = str_replace($placeholder, "", $query);
				continue;
			}
			
			$parts = [];
			
			foreach(array_keys($bindings["bindings"]) as $key) {
				$parts[] = " $key=:$key ";
			}
			
			// ["x=:x", "y=:y"] -> "x=:x AND y=:y"
			$replacement  = $bindings["prepend"];
			$replacement .= implode($bindings["joint"], $parts);
			$replacement .= $bindings["append"];
			
			// "WHERE %w" -> "WHERE x=:x AND y=:y"
			$query = str_replace($placeholder, $replacement, $query);
		}
		$stmt = $this->db->prepare($query);
		
		foreach($this->bindings as $placeholder => $bindings) {
			foreach($bindings["bindings"] as $key => $binding) {
				$stmt->bindValue(":$key", $binding["value"], $binding["type"]);
			}
		}
		
		return $stmt;
	}
}
