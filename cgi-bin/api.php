<?php
/*<form method="post" autocomplete="off">
	<input type="text"     name="user" placeholder="Username" />
	<input type="password" name="pass" placeholder="Password" />
	
	<input type="submit" name="submit" value="Sesam &ouml;ffne dich!" />
	<input type="hidden" name="action" value="unlock" />
</form>

<form method="post" autocomplete="off">
	<input type="text"     name="user" placeholder="Username" />
	<input type="password" name="pass" placeholder="Altes Passwort" />
	<input type="password" name="newpass1" placeholder="Neues Passwort" />
	<input type="password" name="newpass2" placeholder="Neues Passwort wiederholen" />
	
	<input type="submit" name="submit" value="Passwort ändern" />
	<input type="hidden" name="action" value="changePassword" />
</form>

<form method="post" autocomplete="off">
	<input type="text" name="user" placeholder="Username" />
	<input type="password" name="pass" placeholder="Password"/>
	
	<input type="submit" name="submit" value="Manager login" />
	<input type="hidden" name="action" value="login" />
</form>

<form method="post" autocomplete="off">
	<input type="submit" name="submit" value="Manager logout" />
	<input type="hidden" name="action" value="logout" />
</form>

<form method="post" autocomplete="off">
	<input type="text"     name="id"  placeholder="ID" />
	<input type="text"     name="user" placeholder="Username" />
	<input type="password" name="pass" placeholder="Neues Passwort" />
	<input type="checkbox" name="manager" value="manager" /><label for="manager">Manager</label>
	
	<input type="submit" name="submit" value="Update user" />
	<input type="hidden" name="action" value="updateUser" />
</form>

<form method="post" autocomplete="off">
	<input type="text"     name="user" placeholder="Username" />
	<input type="password" name="pass" placeholder="Neues Passwort" />
	<input type="checkbox" name="manager" value="manager" /><label for="manager">Manager</label>
	
	<input type="submit" name="submit" value="Create user" />
	<input type="hidden" name="action" value="createUser" />
</form>

<form method="post" autocomplete="off">
	<input type="text"     name="id"  placeholder="ID" />
	
	<input type="submit" name="submit" value="Delete user" />
	<input type="hidden" name="action" value="deleteUser" />
</form>

<form method="post" autocomplete="off">
	<input type="text"     name="id"  placeholder="ID" />
	
	<input type="submit" name="submit" value="Get user" />
	<input type="hidden" name="action" value="getUser" />
</form>

<form method="post" autocomplete="off">
	<input type="submit" name="submit" value="Get users" />
	<input type="hidden" name="action" value="getUsers" />
</form>*/




include "Db.php";



$db = new Db("db");

function unlock() {
	global $db;
	
	if(!isset($_POST['user']) || !isset($_POST['pass'])) {
		throw new Exception("Input error");
	}
	
	$users = $db->getUsers(null, null, $_POST['user'], $_POST['pass'], true);
	if(count($users) !== 1) {
		throw new Exception("Invalid user or password");
	}
	
	$db->createLog($users[0]["id"], Db::$LOG_UNLOCK);
	
	//echo "triggered!";
	return [];
}

function changePassword() {
	global $db;
	
	if(
		!isset($_POST['user']) ||
		!isset($_POST['pass']) ||
		!isset($_POST['newpass1']) ||
		!isset($_POST['newpass2']))
	{
		throw new Exception("Input error");
	}
	
	if($_POST['newpass1'] !== $_POST['newpass2']) {
		throw new Exception("New Passwort mismatch");
	}
	
	$users = $db->getUsers(null, null, $_POST['user'], $_POST['pass'], true);
	if(count($users) !== 1) {
		throw new Exception("Invalid user or password");
	}
	
	$db->updateUser($users[0]["id"], null, null, $_POST['newpass1']);
	
	$db->createLog($users[0]["id"], Db::$LOG_CHANGE_PASSWORD);
	
	return [];
}

function login() {
	global $db;
	
	if(!isset($_POST['user']) || !isset($_POST['pass'])) {
		throw new Exception("Input error");
	}
	
	$users = $db->getUsers(null, null, $_POST['user'], $_POST['pass'], true);
	if(count($users) !== 1) {
		throw new Exception("Invalid user or password");
	}
	
	if($users[0]["manager"] !== true) {
		throw new Exception("Sorry, you are not a manager");
	}
	
	$_SESSION['uid'] = $users[0]["id"];
	
	$db->createLog($users[0]["id"], Db::$LOG_LOGIN);
	
	return ["id" => $users[0]["id"]];
}

function logout() {
	unset($_SESSION['uid']);
	return [];
}

function updateUser() {
	global $db;
	
	if(
		!isset($_POST['id']) ||
		!isset($_POST['name']) ||
		!isset($_POST['note'])
	) {
		throw new Exception("Input error");
	}
	
	if(ctype_digit($_POST['id']) === false) {
		throw new Exception("Id is not a number");
	}
	
	$manager = getManager();
	if($manager === null) {
		throw new Exception("Only authentificated managers can do this");
	}
	
	$id = (int)$_POST['id'];
	$pass = !isset($_POST['pass']) || $_POST['pass'] === "" ? null : $_POST['pass'];
	$user = !isset($_POST['user']) ? null : $_POST['user'];
	
	if($id === $manager["id"]) {
		if($user !== null) {
			throw new Exception("Sorry, you can't change your username here");
		}
		
		if($pass !== null) {
			throw new Exception("Sorry, you can't change your password here");
		}
	}
	
	$db->updateUser($id, $_POST['name'], $user, $pass, null, null, $_POST['note']);
	
	$db->createLog($manager["id"], Db::$LOG_UPDATE_USER, $id);
	
	return [];
}



function setEnabled($isEnabled) {
	global $db;
	
	if(!isset($_POST['id'])) {
		throw new Exception("Input error");
	}
	
	if(ctype_digit($_POST['id']) === false) {
		throw new Exception("Id is not a number");
	}
	
	$manager = getManager();
	if($manager === null) {
		throw new Exception("Only authentificated managers can do this");
	}
	
	$id = (int)$_POST['id'];
	
	if($id === $manager["id"]) {
		throw new Exception("Sorry, you can't degrade yourself");
	}
	
	$db->updateUser($id, null, null, null, $isEnabled);
	
	if($isEnabled) {
		$db->createLog($manager["id"], Db::$LOG_ENABLE_USER, $id);
	}
	else {
		$db->createLog($manager["id"], Db::$LOG_DISABLE_USER, $id);
	}
	
	return [];
}



function setManager($isManager) {
	global $db;
	
	if(!isset($_POST['id'])) {
		throw new Exception("Input error");
	}
	
	if(ctype_digit($_POST['id']) === false) {
		throw new Exception("Id is not a number");
	}
	
	$manager = getManager();
	if($manager === null) {
		throw new Exception("Only authentificated managers can do this");
	}
	
	$id = (int)$_POST['id'];
	
	if($id === $manager["id"]) {
		throw new Exception("Sorry, you can't degrade yourself");
	}
	
	$db->updateUser($id, null, null, null, null, $isManager);
	
	if($isManager) {
		$db->createLog($manager["id"], Db::$LOG_PROMOTE_USER, $id);
	}
	else {
		$db->createLog($manager["id"], Db::$LOG_DEGRADE_USER, $id);
	}
	
	return [];
}



function createUser() {
	global $db;
	
	if(
		!isset($_POST['name']) ||
		!isset($_POST['user']) ||
		!isset($_POST['pass']) ||
		!isset($_POST['note'])) {
		throw new Exception("Input error");
	}
	
	$manager = getManager();
	if($manager === null) {
		throw new Exception("Only authentificated managers can do this");
	}
	
	$id = $db->createUser(
		$_POST['name'], $_POST['user'], $_POST['pass'], 
		true, false, $_POST['note']
	);
	
	$db->createLog($manager["id"], Db::$LOG_CREATE_USER, $id);
	
	return ["id" => $id];
}


function deleteUser() {
	global $db;
	
	if(!isset($_POST['id'])) {
		throw new Exception("Input error");
	}
	
	if(ctype_digit($_POST['id']) === false) {
		throw new Exception("Id is not a number");
	}
	
	$id = (int)$_POST['id'];
	
	$manager = getManager();
	if($manager === null) {
		throw new Exception("Only authentificated managers can do this");
	}
	
	if($id === $manager["id"]) {
		throw new Exception("You can't  delete yourself");
	}
	
	$users = $db->getUsers($id);
	if(count($users) !== 1) {
		throw new Exception("User does not exist");
	}
	
	$db->createLog($manager["id"], Db::$LOG_DELETE_USER, null, $users["name"]);
	
	$db->deleteUser($id, true);
	
	return [];
}


function getUser() {
	global $db;
	
	if(!isset($_POST['id'])) {
		throw new Exception("Input error");
	}
	
	if(ctype_digit($_POST['id']) === false) {
		throw new Exception("Id is not a number");
	}
	
	$id = (int)$_POST['id'];
	
	$manager = getManager();
	if($manager === null) {
		throw new Exception("Only authentificated managers can do this");
	}
	
	$users = $db->getUsers($id);
	if(count($users) !== 1) {
		throw new Exception("User does not exist");
	}
	
	return $users[0];
}


function getUsers() {
	global $db;
	
	$manager = getManager();
	if($manager === null) {
		throw new Exception("Only authentificated managers can do this");
	}
	
	return $db->getUsers();
}


function getManager() {
	global $db;
	
	if(isset($_SESSION['uid']) === false) {
		return null;
	}
	
	$users = $db->getUsers($_SESSION['uid'], null, null, true);
	if(count($users) !== 1) {
		return null;
	}
	
	return $users[0];
}



function getData() {
	if(!isset($_POST['action'])) {
		return null;
	}
	
	switch($_POST['action']) {
		case "unlock":         return unlock();
		case "changePassword": return changePassword();
		
		case "login":          return login();
		case "logout":         return logout();
		
		case "createUser":     return createUser();
		case "deleteUser":     return deleteUser();
		
		case "updateUser":     return updateUser();
		
		case "enableUser":     return setEnabled(true);
		case "disableUser":    return setEnabled(false);
		
		case "promoteUser":    return setManager(true);
		case "degradeUser":    return setManager(false);
		
		case "getUser":        return getUser();
		case "getUsers":       return getUsers();
	}
}

function main() {
	session_start();
	
	try {
		echo json_encode(["data" => getData()]);
	}
	catch(Exception $e) {
		echo json_encode(["errors" => ["user" => [$e->getMessage()]]]);
	}
// 	echo "<h1>Result dataset</h1>";
// 	var_dump(getData());
// 	
// 	echo "<h1>Session data</h1>";
// 	var_dump($_SESSION);
}

main();

