<?php

include "QueryBuilder.php";


class Db {
	public static $LOG_UNLOCK          = 0;
	public static $LOG_CHANGE_PASSWORD = 1;
	
	public static $LOG_LOGIN           = 2;
	
	public static $LOG_CREATE_USER     = 3;
	public static $LOG_DELETE_USER     = 4;
	
	public static $LOG_UPDATE_USER     = 5;
	
	public static $LOG_ENABLE_USER     = 6;
	public static $LOG_DISABLE_USER    = 7;
	
	public static $LOG_PROMOTE_USER    = 8;
	public static $LOG_DEGRADE_USER    = 9;
	
	private $db = null;
	
	
	public function __construct($file) {
		$this->db = new SQLite3($file);
		$this->db->exec('PRAGMA foreign_keys = ON');
	}
	
	private function execute($stmt) {
		$result = $stmt->execute();
		
		if($result === false) {
			throw new Exception("Can't execute query");
		}
		
		return $result;
	}
	
	
	public function updateUser(
		$id, $name = null, $user = null, $pass = null,
		$enabled = null, $manager = null, $note = null
	) {
		if($pass !== null) {
			$pass = crypt($pass);
		}
		
		$qb = new QueryBuilder($this->db, 'UPDATE users SET %s WHERE id=:id');
		$qb->addBindingList("%s", ", ");
		$qb->addBinding("%s", "name",    $name,    SQLITE3_TEXT,    true);
		$qb->addBinding("%s", "user",    $user,    SQLITE3_TEXT,    true);
		$qb->addBinding("%s", "pass",    $pass,    SQLITE3_TEXT,    true);
		$qb->addBinding("%s", "enabled", $enabled, SQLITE3_INTEGER, true);
		$qb->addBinding("%s", "manager", $manager, SQLITE3_INTEGER, true);
		$qb->addBinding("%s", "note",    $note,    SQLITE3_TEXT,    true);
		$stmt = $qb->build();
		$stmt->bindValue(':id', $id, SQLITE3_INTEGER);
		
		$result = $this->execute($stmt);
		
		if($this->db->changes() !== 1) {
			throw new Exception("The user does not exist");
		}
	}

	public function createUser(
		$name, $user, $pass,
		$enabled = null, $manager = null, $note = null
	) {
		$pass = crypt($pass);
		
		$stmt = $this->db->prepare(
			'INSERT INTO users ('.
				'name, '.
				'user, '.
				'pass, '.
				'enabled, '.
				'manager, '.
				'note'.
			') '.
			'VALUES ('.
				':name, '.
				':user, '.
				':pass, '.
				':enabled, '.
				':manager, '.
				':note'.
			')'
		);
		$stmt->bindValue(":name",    $name,    SQLITE3_TEXT);
		$stmt->bindValue(":user",    $user,    SQLITE3_TEXT);
		$stmt->bindValue(":pass",    $pass,    SQLITE3_TEXT);
		$stmt->bindValue(":enabled", $enabled, SQLITE3_INTEGER);
		$stmt->bindValue(":manager", $manager, SQLITE3_INTEGER);
		$stmt->bindValue(":note",    $note,    SQLITE3_TEXT);
		
		$result = $this->execute($stmt);
		
		return $this->db->lastInsertRowID();
	}

	public function deleteUser($id, $force = false) {
		if($force === true) {
			$stmt = $this->db->prepare(
				'DELETE FROM logs '.
				'WHERE subject=:id OR object=:id'
			);
			
			$stmt->bindValue(":id", $id, SQLITE3_INTEGER);
			$result = $this->execute($stmt);
		}
		
		$stmt = $this->db->prepare('DELETE FROM users WHERE id=:id');
		$stmt->bindValue(":id", $id, SQLITE3_INTEGER);
		
		$result = $this->execute($stmt);
		
		if($this->db->changes() !== 1) {
			throw new Exception("The user does not exist");
		}
	}
	
	
	public function getUsers(
		$id = null, $name = null, $user = null, $pass = null, 
		$enabled = null, $manager = null, $note = null
	) {
		// Try to get the user from the DB
		$qb = new QueryBuilder($this->db,
			'SELECT 
				id, 
				name, 
				user, 
				pass, 
				enabled, 
				manager, 
				note
			FROM users 
			%w
		');
		
		$qb->addBindingList("%w", "AND", "WHERE");
		$qb->addBinding("%w", "id",      $id,      SQLITE3_INTEGER, true);
		$qb->addBinding("%w", "name",    $name,    SQLITE3_TEXT,    true);
		$qb->addBinding("%w", "user",    $user,    SQLITE3_TEXT,    true);
		$qb->addBinding("%w", "enabled", $enabled, SQLITE3_INTEGER, true);
		$qb->addBinding("%w", "manager", $manager, SQLITE3_INTEGER, true);
		$qb->addBinding("%w", "note",    $note,    SQLITE3_TEXT,    true);
		$stmt = $qb->build();
		$result = $this->execute($stmt);
		
		$results = [];
		while($row = $result->fetchArray(SQLITE3_ASSOC)) {
			
			// Compare the password
			if($pass === null || crypt($pass, $row['pass']) === $row['pass']) {
				unset($row['pass']);
				$row["manager"] = $row["manager"] === 1; // Be pessemistic
				$results[] = $row;
			}
		}
		
		return $results;
	}
	
	public function createLog($subject, $verb, $object = null, $data = null) {
		if($data !== null) {
			$data = serialize($data);
		}
		
		$stmt = $this->db->prepare(
			'INSERT INTO logs ('.
				'subject, '.
				'verb, '.
				'object, '.
				'data'.
			') '.
			'VALUES ('.
				':subject, '.
				':verb, '.
				':object, '.
				':data'.
			')'
		);
		$stmt->bindValue(":subject", $subject, SQLITE3_INTEGER);
		$stmt->bindValue(":verb",    $verb,    SQLITE3_INTEGER);
		$stmt->bindValue(":object",  $object,  SQLITE3_INTEGER);
		$stmt->bindValue(":data",    $data,    SQLITE3_TEXT);
		
		$result = $this->execute($stmt);
		
		return $this->db->lastInsertRowID();
	}
	
	public function getLogs(
		$id = null, $subject = null, $verb = null, $object = null, $data = null
	) {
		// Try to get the user from the DB
		$qb = new QueryBuilder($this->db,
			'SELECT '.
				'id, '.
				'verb, '.
				'object, '.
				'data, '.
				'date'.
			'FROM logs '.
			'%w'
		);
		
		$qb->addBindingList("%w", "AND", "WHERE");
		$qb->addBinding("%w", "id",      $id,      SQLITE3_INTEGER, true);
		$qb->addBinding("%w", "subject", $subject, SQLITE3_INTEGER, true);
		$qb->addBinding("%w", "verb",    $verb,    SQLITE3_INTEGER, true);
		$qb->addBinding("%w", "object",  $object,  SQLITE3_INTEGER, true);
		$qb->addBinding("%w", "data",    $data,    SQLITE3_TEXT,    true);
		$stmt = $qb->build();
		$result = $this->execute($stmt);
		
		$results = [];
		while($row = $result->fetchArray(SQLITE3_ASSOC)) {
			if($row["data"] !== null) {
				$row["data"] = unserialize($row["data"]);
			}
			
			$results[] = $row;
		}
		
		return $results;
	}
}

